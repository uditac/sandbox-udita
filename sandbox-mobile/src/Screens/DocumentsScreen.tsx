import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { SafeAreaView, StyleSheet, Text } from "react-native";

export function DocumentsScreen() {
    const navigation = useNavigation();

    return (
        <SafeAreaView style={styles.screen}>
            <Text style={styles.title}>Documents</Text>
            <Text style={styles.link} onPress={() => navigation.navigate("DocumentDetails", { testParam: "Implement me..." })}>Click for details</Text>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
    },
    title: {
        fontSize: 20,
    },
    link: {
        padding: 20,
        color: 'blue',
    }
});
